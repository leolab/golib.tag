package Tag

import (
	"bytes"
	"io"
	"os"
	"testing"
)

/*
func TestTag_Simple(t *testing.T) {
	tg := New("Simple")
	print("Tag: " + tg.String() + "\n")
}

func TestTag_Full(t *testing.T) {
	var b bytes.Buffer
	tg := New("Tag", "Mod", "Ext", "Data", "key1=val1")
	print("Tag (tf): " + tg.String() + "\n")
	tn := New("Tag1", "t1k1=v1", *tg)
	print("Tag (tff): " + tn.String() + "\n")
	p := ParserNew()
	b.WriteString(tn.String())
	tgn, e := p.ParseBuf(&b)
	if e != nil {
		t.Error(e)
	}
	if tgn != nil {
		print("Tag (rp): " + tgn.String() + "\n")
	} else {
		print("NIL TAG >> " + p.cTag.String() + "\n")
		t.Error("NIL Tag!!!")
	}
}
*/

func TestTag_FromFile(t *testing.T) {
	fp, e := FileParser_New("./test.tgd")
	if e != nil {
		panic(e)
	}
	for {
		t, e := fp.Get()
		if e != nil {
			if e != io.EOF {
				panic(e)
			} else {
				print("Done (EOF)\n")
				break
			}
		}
		if t != nil {
			print("Tag:" + t.String() + "\n")
		}
	}
}

func _TestTag_FromFile(t *testing.T) {
	//	Dbg = true
	var b bytes.Buffer
	f, e := os.Open("./test.tgd")
	if e != nil {
		t.Error(e)
	}
	//	var er error
	//	var nt *Tag
	//	er = nil
	bf := make([]byte, 64)
	p := ParserNew()
	//	for er != io.EOF {
	//		_, er = f.Read(bf)
	for {
		_, er := f.Read(bf)
		if er != nil {
			if er != io.EOF {
				print("\n\nERROR: " + er.Error() + "\n\n")
				//				panic(er)
			}
			break
		}
		b.Write(bf)
		//		print("TEST.readed:" + b.String() + "\n")
		//		var ep error; ep = nil
		/*		for ep != io.EOF {
				nt, ep = p.ParseBuf(b)
				if ep == io.EOF {
					continue
				}
		*/
		for b.Len() > 0 {
			//			print("TEST.parse round: " + b.String() + "\n")
			nt, ep := p.ParseBuf(&b)
			//		 ep != nil || nt != nil;
			if nt == nil && ep == nil {
				break
			}
			if ep != nil {
				if ep == io.EOF {
					break
				} else {
					print("\n\nERROR: " + ep.Error() + "\n")
					//					t.Error(ep)
					//					panic(ep)
				}
			}
			if nt != nil {
				print("TAG (fs): " + nt.String() + "\n")
			} else {
				break
			}
		}
	}
}
