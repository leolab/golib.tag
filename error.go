// error
package Tag

import (
	"errors"
)

var EInvalidTag = errors.New("Неверный тег.")
var EEmptyTag = errors.New("Empty TagName")
var EInvalidValue = errors.New("Неверное значение.")
var EInvalidType = errors.New("Неверный тип.")
var ERangeError = errors.New("Ошибка диапазона.")
var ENoKey = errors.New("Ключ не найден.")

type EParseError struct {
	msg string
}

func (e *EParseError) Error() string {
	return e.msg
}

func NewParseError(m string) *EParseError {
	return &EParseError{msg: m}
}
