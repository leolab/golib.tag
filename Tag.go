// Tag
package Tag

import (
	"bytes"
	//	"io"
	"strconv"
	"strings"
	//	"unicode"
)

const Version = "1.1"

var Dbg bool

type Tag struct {
	Name string
	Mod  string
	Ext  string
	Data string
	Pars map[string]string
	Tags map[string][]*Tag
	rn   string
}

var rps = strings.NewReplacer("\"", "\\\"", "\\", "\\\\")

//== Вспомогательные методы >>
/*
	Преобразовать значение в строку
*/
func ToString(v interface{}) string {
	switch v.(type) {
	case string:
		return v.(string)
	case int:
		return strconv.FormatInt(int64(v.(int)), 10)
	case int8:
		return strconv.FormatInt(int64(v.(int8)), 10)
	case int16:
		return strconv.FormatInt(int64(v.(int16)), 10)
	case int32:
		return strconv.FormatInt(int64(v.(int32)), 10)
	case int64:
		return strconv.FormatInt(v.(int64), 10)
	case float32:
		return strconv.FormatFloat(float64(v.(float32)), 'G', 5, 32)
	case float64:
		return strconv.FormatFloat(v.(float64), 'G', 5, 64)
	case uint:
		return strconv.FormatUint(uint64(v.(uint)), 10)
	case uint8:
		return strconv.FormatUint(uint64(v.(uint8)), 10)
	case uint16:
		return strconv.FormatUint(uint64(v.(uint16)), 10)
	case uint32:
		return strconv.FormatUint(uint64(v.(uint32)), 10)
	case uint64:
		return strconv.FormatUint(v.(uint64), 10)
	case bool:
		if v.(bool) {
			return "true"
		} else {
			return "false"
		}
	default:
	}
	return ""
}

func Lc(s string) string {
	return strings.ToLower(s)
}

func Uc(s string) string {
	return strings.ToUpper(s)
}

func StrToInt(s string) int {
	i, _ := strconv.ParseInt(s, 10, 0)
	return int(i)
}

func StrToInt8(s string) int8 {
	i, _ := strconv.ParseInt(s, 10, 0)
	return int8(i)
}

func StrToInt16(s string) int16 {
	i, _ := strconv.ParseInt(s, 10, 0)
	return int16(i)
}

func StrToInt32(s string) int32 {
	i, _ := strconv.ParseInt(s, 10, 0)
	return int32(i)
}

func StrToInt64(s string) int64 {
	i, _ := strconv.ParseInt(s, 10, 0)
	return int64(i)
}

func StrToFloat32(s string) float32 {
	f, _ := strconv.ParseFloat(s, 32)
	return float32(f)
}

func StrToFloat64(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}

//<< Вспомогательные методы ==

/*
	Конструктор пустого тега
(-) Deprecated.
*/
/*func NewTag() *Tag {
	return New()
}*/

/*
	Конструктор
	t := Tag.New(string,interface{},map[string]string],Tag...)
*/
func New(pars ...interface{}) *Tag {
	l := len(pars)
	t := &Tag{Name: "", Mod: "", Ext: "", Data: "", Pars: make(map[string]string), Tags: make(map[string][]*Tag)}
	if l > 0 {
		t.Name = pars[0].(string)
	} else {
		return t
	}
	if l > 1 {
		for i := 1; i < l; i++ {
			switch pars[i].(type) {
			case string: //Параметр вида key=val
				if strings.ContainsRune(pars[i].(string), '=') {
					ks := strings.SplitN(pars[i].(string), "=", 2)
					t.SetParam(ks[0], ks[1])
				} else {
					switch i {
					default:
						t.SetParam(pars[i].(string), "")
					case 1:
						t.Mod = pars[i].(string)
					case 2:
						t.Ext = pars[i].(string)
					case 3:
						t.Data = pars[i].(string)
					}
				}
			case map[interface{}]interface{}: //Массив параметров
				for k, v := range pars[i].(map[interface{}]interface{}) {
					t.SetParam(k, v)
				}
			case *Tag: //Тег
				t.AddTag(pars[i].(*Tag))
			case Tag:
				tg := pars[i].(Tag)
				t.AddTag(&tg)
			case int, int8, int16, int32, int64, float32, float64:
				switch i {
				default:
					t.SetParam(pars[i], "")
				case 1:
					t.Mod = ToString(pars[i])
				case 2:
					t.Ext = ToString(pars[i])
				case 3:
					t.Data = ToString(pars[i])
				}
			default: //Wrong type
			}
		}
	}
	return t
}

/*
	Добавить вложенный тег
*/
func (t *Tag) AddTag(tag *Tag) {
	t.Tags[tag.Name] = append(t.Tags[tag.Name], tag)
}

/*
	Удалить тег (по индексу)
*/
func (t *Tag) DelTag(tName string, idx int) {
	t.Tags[tName][idx] = t.Tags[tName][len(t.Tags[tName])-1]
	t.Tags[tName] = t.Tags[tName][0 : len(t.Tags[tName])-1]
}

/*
	Добавить или изменить параметр
*/
func (t *Tag) SetParam(k interface{}, v interface{}) error {
	key := ToString(k)
	if key == "" {
		return ENoKey
	}
	val := ToString(v)
	t.Pars[key] = val
	return nil
}

/*
	Получить строковое значение параметра
*/
func (t *Tag) GetParam(k interface{}) string {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		return t.Pars[key]
	}
	return ""
}

/*
	Найти имена параметров без учера регистра
*/
func (t *Tag) FindParams(k interface{}) []string {
	var r []string
	key := ToString(k)
	if key == "" {
		return nil
	}
	key = Lc(key)
	for k, _ := range t.Pars {
		if key == Lc(k) {
			r = append(r, k)
		}
	}
	return r
}

/*
	Найти имена тегов без учета регистра
*/

func (t *Tag) FindTags(k interface{}) []string {
	var r []string
	key := ToString(k)
	if key == "" {
		return nil
	}
	key = Lc(key)
	for k, _ := range t.Tags {
		if key == Lc(k) {
			r = append(r, k)
		}
	}
	return r
}

func (t *Tag) ParamExists(k interface{}) bool {
	key := ToString(k)
	if key == "" {
		return false
	}
	if _, ok := t.Pars[key]; ok {
		return true
	}
	return false
}

func (t *Tag) GetParam_int(k interface{}) int {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		i, _ := strconv.ParseInt(t.Pars[key], 10, 0)
		return int(i)
	}
	return 0
}

func (t *Tag) GetParam_int16(k interface{}) int16 {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		i, _ := strconv.ParseInt(t.Pars[key], 10, 16)
		return int16(i)
	}
	return 0
}

func (t *Tag) GetParam_int32(k interface{}) int32 {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		i, _ := strconv.ParseInt(t.Pars[key], 10, 32)
		return int32(i)
	}
	return 0
}

func (t *Tag) GetParam_int64(k interface{}) int64 {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		i, _ := strconv.ParseInt(t.Pars[key], 10, 64)
		return int64(i)
	}
	return 0
}

func (t *Tag) GetParam_float32(k interface{}) float32 {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		f, _ := strconv.ParseFloat(t.Pars[key], 32)
		return float32(f)
	}
	return 0
}

func (t *Tag) GetParam_float64(k interface{}) float64 {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		f, _ := strconv.ParseFloat(t.Pars[key], 64)
		return f
	}
	return 0
}

func (t *Tag) GetParam_str(k interface{}) string {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		return t.Pars[key]
	}
	return ""
}

func (t *Tag) GetParam_bool(k interface{}) bool {
	key := ToString(k)
	if v, ok := t.Pars[key]; ok {
		switch strings.ToLower(v) {
		default:
			return false
		case "1", "y", "t", "true", "yes", "on":
			return true
		}
	}
	return false
}

/*
	Удалить параметр
*/
func (t *Tag) DelParam(k interface{}) error {
	key := ToString(k)
	if _, ok := t.Pars[key]; ok {
		delete(t.Pars, key)
	} else {
		return ENoKey
	}
	return nil
}

/*
	Преобразовать тег в строку
*/
func (t *Tag) String() string {
	fin_ch := '.'
	//Имя тега не может содержать определенные символы, а так же быть пустым.
	if t.Name == "" {
		return ""
	}
	if strings.ContainsAny(t.Name, " .;[]{}()@#$%^&*-+!`~'\"\\/,*") {
		return ""
	}
	b := bytes.NewBufferString("@" + t.Name)
	if t.Mod != "" {
		b.WriteString("[" + esc(t.Mod) + "]")
	}
	if t.Ext != "" || t.Data != "" {
		b.WriteString(":")
		if t.Ext != "" {
			b.WriteString("[" + esc(t.Ext) + "]")
		}
		if t.Data != "" {
			b.WriteString(escD(t.Data))
		}
	}
	if len(t.Pars) > 0 || len(t.Tags) > 0 {
		b.WriteRune('{')
		b.WriteString(t.rn)
		if len(t.Pars) > 0 {
			for k, v := range t.Pars {
				b.WriteString(esc(k) + "=" + esc(v) + ";" + t.rn)
			}
		}
		if len(t.Tags) > 0 {
			for _, v := range t.Tags {
				for i := range v {
					b.WriteString(v[i].String() + t.rn)
				}
			}
		}
		b.WriteRune('}')
		//		fin_ch = ';'
	}
	b.WriteRune(fin_ch)
	b.WriteString(t.rn)
	return b.String()
}

/*
	Преобразовать данные в строку, при необходимости обернуть кавычками и заэскапить
*/
func esc(s string) string {
	if strings.ContainsAny(s, " ;[]{}()@#$%^&*+!`~'\"\\/*") {
		return "\"" + rps.Replace(s) + "\""
	}
	return s
}

func escD(s string) string {
	if strings.ContainsAny(s, " .;[]{}()@#$%^&*-+!`~'\"\\/,*") {
		return "\"" + rps.Replace(s) + "\""
	}
	return s
}

func escC(s string) string {
	if strings.ContainsAny(s, " .;[]{}()@#$%^&*-+!`~'\"\\/,*") {
		return rps.Replace(s)
	}
	return s
}
