# Пакет работы с форматом DogTag 2.0

[!] TODO: Привести в соответствие с изменениями.

## Overview:
Пакет реализует методы и структуры обработки сообщений DogTag

## Usage
### Разбор тегов из файла
```
	fp, e := FileParser_New("./test.tgd")
	if e != nil {
		panic(e)
	}
	for {
		t, e := fp.Get()
		if e != nil {
			if e != io.EOF {
				panic(e)
			} else {
				print("Done (EOF)\n")
				break
			}
		}
		if t != nil {
			print("Tag:" + t.String() + "\n")
		}
	}
```
### Разбор тегов из буфера
```
	var b bytes.Buffer
	f, e := os.Open("./test.tgd")
	if e != nil {
		t.Error(e)
	}
	bf := make([]byte, 64)
	p := ParserNew()
	for {
		_, er := f.Read(bf)
		if er != nil {
			if er != io.EOF {
				print("\n\nERROR: " + er.Error() + "\n\n")
			}
			break
		}
		b.Write(bf)
		for b.Len() > 0 {
			nt, ep := p.ParseBuf(&b)
			if nt == nil && ep == nil {
				break
			}
			if ep != nil {
				if ep == io.EOF {
					break
				} else {
					print("\n\nERROR: " + ep.Error() + "\n")
				}
			}
			if nt != nil {
				print("TAG (fs): " + nt.String() + "\n")
			} else {
				break
			}
		}
	}
```

### Методы
* _*Tag_ Tag.New(_string_,_interface{}_...)
* _*Tag_,_error_ Tag.FromString(_string_)
* _error_ Tag.FromBuff(_bytes.Buffer_,_*Tag_)
* _string_,_error_ T.String()
* T.AddTag(_*Tag_)
* T.DelTag(_string_,_int_)
* T.SetParam(_variant_,_variant_)
* _string_ T.GetParam(_variant_)
* _string_,_error_ T.GetParam_str()
* _int_,_error_ T.GetParam_int()****
* _int32_,_error_ T.GetParam_int32()
* _int64_,_error_ T.GetParam_int64()
* _float32_,_error_ T.GetParam_float32()
* _float64_,_error_ T.SetParam_float64()

#### Заполнить тег из буфера
`func TagFromBuff(*bytes.Buffer, *TParseMode, *Tag) error`
Заполняет параметры тега из буфера.

:!?: - привести к виду: func TagFromBuff(*bytes.Buffer,*TParseMode,*Tag) (*Tag, error)

При error == nil - тег создан (в буфере еще могут быть данные)

При error == io.EOF - буфер пуст, но тег еще не создан (не завершен).

#### Создать новый тег

`func New(Name string,[Mod string,[string("key=val")],Tag]) *Tag`
Создает новый тег.
##### Использование:
- создание пустого тега: `nt:=Tag.New()`
- создание тега с именем: `nt:=Tag.New("TagName")`
- создание тега с именем и модификатором: `nt:=Tag.New("TagName",TagMod)`
- создание тега с именем и параметрами: `nt:=Tag.New("TagName","","key1=val1","key2=val2",map[interface{}]interface{}{"key3":123,"key4":"val4")
- создание тега с именем, модификатором, параметрами и вложенными тегами:
```

nt:=Tag.New(
	"TagName",
	TagMod,
	"key1=val1","key2=val2",
	Tag.New("SubTag","","sk1=sv1"),
	Tag.New("SubTag","mod","sk2=sv2"),
)
```

#### Создать тег из строки
`func NewTagFromString(string) (*Tag, error)`
Создает новый тег из строки.

:!: Метод создает только один тег. Если строка содержит более одного тега - будет создан только первый, включая все вложенные.

### Типы:
* Tag _struct_
  * Name _string_
  * Mod _string_
  * Ext _string_
  * Data _string_
  * Pars _map[string]string_
  * Tags _map[string][]*Tag_
  * SetParam(key _interface{}_, val _interface{}_) _error_ - Добавить или изменить параметр. Допустимо использовать типы ключей: uint,int,float,string, типы значений: uint;uint8;uint16;uint32;uint64;int;int8;int16;int32;int64;float32;float64;string;bool.
  * DelParam(key _interface{}_) _error_ - Удалить параметр
  * [-] GetParam(key _interface{}_) _string_ - Получить значение параметра. :!: Метод для отсутствующего параметра вернет пустую строку. Для получения *реального* значения параметра необходимо использовать метод: GetParam_str()
  * GetParam_int(key _interface{}_) (_int_, _error_) - Получить параметр типа int
  * GetParam_int32(key _interface{}_) (_int32_,_error_) - Получить параметр типа int32
  * GetParam_int64(key _interface{}_) (_int64_, _error_) - Получить параметр типа int64
  * GetParam_float32(key _interface{}_) (_float32_,_error_) - Получить параметр типа float32
  * GetParam_float64(key _interface{}_) (_float64_,_error_) - Получить параметр типа float64
  * GetParam_str(key _interface{}_) (_string_, _error_) - Получить параметр строкового типа (при необходимости будет преобразован)
  * AddTag(t _*Tag_) - добавить вложенный тег
  * DelTag(Name _string_,idx _int_) - Удалить вложенный тег
  * String() (_string_,_error_) - Вернуть строковое представление тега

## Задачи
### KnownBugs:
### ToDo:
#### [#1+] Реализовать обратную совместимость с DogTag 1.x - возможность разбора сообщений старого формата.

```
@Tag[Mod]:[Ext]Data

@Tag[Mod]:[Ext]Begin
key=val
@SubTag
@Tag[Mod]:[Ext]End
```

При этом поля старого формата во внутреннее представление отображаются как:
* Tag -> T.Name
* Mod -> T.Mod
* Ext -> T.Ext
* Data -> T.Data

#### [#2] Реализовать загрузку массива тегов из локального файла.

```
MyTags := Tag.FromFile(FileName string)
for t,n := range(MyTags){
	//Обработка тегов.
}
```
# Описание формата
Данные передаются сообщениями

Сообщение состоит из:
* имени сообщения (тега)
* модификатора
* параметров и (или) вложенных сообщений

Сообщение начинается с символа "@".

Модификатор заключается в квадратные скобки.

Параметры и вложенные сообщения заключаются в фигурные скобки.

Параметр состоит из имени (ключа) и значения.

Значение может быть заключено в кавычки.

Параметры отделяются символом ";" (точка с запятой)

Сообщение может состоять только из имени. В этом случае сообщение должно заканчиваться символом "." (точка) или переводом строки.

Модификатор сообщения может отсутствовать.

**TAG:**
 - **@**  _- начало тега_
 - **name**  _- имя тега_
 - [
  - mod _- модификатор тега_
 - ]
 - :
 - [
  - ext _- расшинение_
 - ]
 - data _- данные_
 - {
  - key=val; _- параметр_
  - @tag _- вложенный тег_
 - }
 - .



### Примеры

```
@tag.

@tag[mod].

@tag{..}

@tag[mod]{..}

@tag[mod]:data

@tag[mod]:data{..}

@tag[mod]:[ext]

@tag[mod]:[ext]{..}

@tag[mod]:[ext]data{..}

//Для совместимости с версией 1.0
@tag[mod]:[ext]begin
..
@tag[mod]:[ext]end
```

Параметры (содержание) тега:
```
{
	key=value;
	key:[val1;val2;..;valN];
	@tag.
}
```
