package Tag

/*
Парсер из файла
*/

import (
	"bytes"
	"errors"
	//	"io"
	"os"
)

type FileParser struct {
	P     *Parser
	F     *os.File
	B     bytes.Buffer
	fName string
}

func FileParser_New(fn string) (fp *FileParser, e error) {
	fp = &FileParser{
		P:     ParserNew(),
		fName: fn,
	}
	fp.F, e = os.Open(fp.fName)
	if e != nil {
		return nil, e
	}
	return fp, nil
}

func (fp *FileParser) Close() {
	fp.F.Close()
}

func (fp *FileParser) Get() (t *Tag, e error) {
	bf := make([]byte, 512)
	for {
		if fp.B.Len() < 8 {
			br, e := fp.F.Read(bf)
			if e != nil {
				return nil, e
			}
			fp.B.Write(bf[:br])
			if Dbg {
				print("&[LFC/lib/Tag:parser.FileParser] Readed " + ToString(br) + " bytes, in Buff " + ToString(fp.B.Len()) + " bytes\n")
			}
		} else {
			if Dbg {
				print("&[LFC/lib/Tag:parser.FileParser] in Buff " + ToString(fp.B.Len()) + " bytes.\n")
			}
		}
		if Dbg {
			print("&[LFC/lib/Tag:parser.FileParser] Parse\n")
		}
		t, e = fp.P.ParseBuf(&fp.B)
		if e != nil {
			return nil, e
		}
		if t != nil {
			return t, nil
		}
	}
	return nil, errors.New("Unexpected exit.")
}
