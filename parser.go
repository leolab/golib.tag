package Tag

import (
	"bytes"
	//	"errors"
	"io"
	"unicode"
	"unicode/utf8"
)

func pmToString(pm pMode) string {
	switch pm {
	case pmSkip:
		return "skip"
	case pmName:
		return "name"
	case pmMod:
		return "mod"
	case pmExtWt:
		return "extWt"
	case pmExt:
		return "ext"
	case pmData:
		return "data"
	case pmParamKey:
		return "pKey"
	case pmParamVal:
		return "pVal"
	case pmTag:
		return "tag"
	default:
		return "unknown"
	}
}

type pMode int8

const (
	pmSkip pMode = iota
	pmName
	pmMod
	pmExtWt
	pmExt
	pmData
	pmParamKey
	pmParamVal
	pmTag

//	pmComm
)

func epmToString(e epMode) string {
	switch e {
	case epmNone:
		return ""
	case epmEsc:
		return "esc"
	case epmCommSt:
		return "comm_s"
	case epmComm:
		return "comm"
	case epmCommFin:
		return "comm_f"
	default:
		return "unknown"
	}
}

type epMode int8

const (
	epmNone    epMode = iota
	epmEsc            //Esc-символ
	epmStr            //Строка
	epmCommSt         //Начало комментария, следующий символ - *, предыдущий - (
	epmComm           //Внутри комментария
	epmCommFin        //Конец комментария, следующий символ - ), предыдущий - *
)

type Parser struct {
	cTag   *Tag
	cpMode pMode
	cpmExt epMode
	bs     bytes.Buffer
	pKey   string
	sP     *Parser
}

func ParserNew() *Parser {
	return &Parser{cTag: nil, cpMode: pmSkip}
}

func (p *Parser) ParseBuf(b *bytes.Buffer) (*Tag, error) {
	if p.cTag == nil {
		p.cTag = New()
		p.bs.Reset()
		p.pKey = ""
	}

	if Dbg {
		print("&[LFC/lib/Tag:parser.ParseBuf] Iterate width " + ToString(b.Len()) + " bytes buffer. Mode: (" + epmToString(p.cpmExt) + ")" + pmToString(p.cpMode) + "\n")
	}

	for {
		r, s, e := b.ReadRune()
		if e != nil {
			if e == io.EOF {
				return nil, nil
			}
			return nil, e
		}
		//Прочитано s байт.
		if s == 0 { // Прочитано 0 байт !?
			return nil, nil
		}
		if r == utf8.RuneError {
			b.UnreadRune()
			if Dbg {
				print("&[LFC/lib/Tag:parser.ParseBuf] RuneError, Mode: (" + epmToString(p.cpmExt) + ")" + pmToString(p.cpMode) + "\n")
			}
			return nil, nil
		}
		if Dbg {
			print("&[LFC/lib/Tag:parser.ParseBuf] Mode: (" + epmToString(p.cpmExt) + ")" + pmToString(p.cpMode) + ", Char: " + string(r) + ", Len: " + ToString(s) + "\n")
		}

		switch p.cpmExt {
		case epmNone: //Не в расширенном режиме
			switch p.cpMode {
			case pmTag: //Вложенный тег
				b.UnreadRune()
				if p.sP == nil {
					p.sP = ParserNew()
				}
				st, e := p.sP.ParseBuf(b)
				if e != nil {
					p.cpMode = pmSkip
					p.sP = nil
					return nil, e
				}
				if st != nil {
					p.cTag.AddTag(st)
					p.sP = nil
					p.cpMode = pmParamKey
				} else {
					return nil, nil
				}
			case pmSkip: //Ждем начала тега
				switch r {
				case '/':
					p.cpmExt = epmCommSt
				case '@':
					p.cpMode = pmName
				default:
				}
			case pmName: //Имя тега
				switch r {
				case '/':
					p.cpmExt = epmCommSt
				case '{': // -> Параметры
					if p.bs.Len() > 0 {
						p.cTag.Name = p.bs.String()
						p.bs.Reset()
						p.cpMode = pmParamKey
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				case '[': // -> Модификатор
					if p.bs.Len() > 0 {
						p.cTag.Name = p.bs.String()
						p.bs.Reset()
						p.cpMode = pmMod
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				case ':': // -> Расширение или данные
					if p.bs.Len() > 0 {
						p.cTag.Name = p.bs.String()
						p.bs.Reset()
						p.cpMode = pmData
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				case '.', ';', '\n', '\r', ' ': //Конец имени тега, тег состоит только из имени.
					if p.bs.Len() > 0 {
						p.cTag.Name = p.bs.String()
						p.bs.Reset()
						p.cpMode = pmSkip
						t := p.cTag
						p.cTag = nil
						return t, nil
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				case '_':
					if p.bs.Len() > 0 {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				default:
					if unicode.IsDigit(r) || unicode.IsLetter(r) {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				}
			case pmMod:
				switch r {
				case '/':
					p.cpmExt = epmCommSt
				case '"':
					p.cpmExt = epmStr
				case ']': // -> Расширение, данные или параметры
					if p.bs.Len() > 0 {
						p.cTag.Mod = p.bs.String()
						p.bs.Reset()
					}
					p.cpMode = pmExtWt
				/*
					case '_':
						if p.bs.Len() > 0 {
							p.bs.WriteRune(r)
						} else {
							sbc := 32
							if len(b.Bytes()) < sbc {
								sbc = len(b.Bytes()) - 1
							}
							e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
							p.cpMode = pmSkip
							return nil, e
						}
				*/
				case '_', '-', '.', ',':
					p.bs.WriteRune(r)
				default:
					if unicode.IsDigit(r) || unicode.IsLetter(r) {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				}
			case pmExtWt:
				switch r {
				case '/':
					p.cpmExt = epmCommSt
				case ':': //-> Расширение или данные
					p.cpMode = pmData
				case '{': // -> Параметры
					p.cpMode = pmParamKey
				case '.', ';', '\n', '\r': //Конец тега.
					p.cpMode = pmSkip
					t := p.cTag
					p.cTag = nil
					return t, nil
				default:
					sbc := 32
					if len(b.Bytes()) < sbc {
						sbc = len(b.Bytes()) - 1
					}
					e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
					p.cpMode = pmSkip
					return nil, e
				}
			case pmExt:
				switch r {
				case '/':
					p.cpmExt = epmCommSt
				case ':', '[': //IMPOSSIBLE
					sbc := 32
					if len(b.Bytes()) < sbc {
						sbc = len(b.Bytes()) - 1
					}
					e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
					p.cpMode = pmSkip
					return nil, e
				case ']': //End of mod, data or params expected
					if p.bs.Len() > 0 {
						p.cTag.Ext = p.bs.String()
						p.bs.Reset()
					}
					p.cpMode = pmData
				case '"': // -> EStr
					if p.bs.Len() > 0 { //IMPOSSIBLE
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
					p.cpmExt = epmStr
				/*
					case '_':
						if p.bs.Len() > 0 {
							p.bs.WriteRune(r)
						} else {
							sbc := 32
							if len(b.Bytes()) < sbc {
								sbc = len(b.Bytes()) - 1
							}
							e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
							p.cpMode = pmSkip
							return nil, e
						}
				*/
				case '_', '.', ',', '-':
					p.bs.WriteRune(r)
				default:
					if unicode.IsDigit(r) || unicode.IsLetter(r) {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				}
			case pmData:
				switch r {
				case '/':
					p.cpmExt = epmCommSt
				case ':', ' ', '\t', '@': //IMPOSSIBLE
					sbc := 32
					if len(b.Bytes()) < sbc {
						sbc = len(b.Bytes()) - 1
					}
					e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
					p.cpMode = pmSkip
					return nil, e
				case '.', ';', '\n', '\r': // -> Finish
					if p.bs.Len() > 0 {
						p.cTag.Data = p.bs.String()
						p.bs.Reset()
					}
					p.cpMode = pmSkip
					t := p.cTag
					p.cTag = nil
					return t, nil
				case '[': // -> Ext, if first time
					if p.bs.Len() > 0 {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
					p.cpMode = pmExt
				case '{': // -> Pars
					if p.bs.Len() > 0 {
						p.cTag.Data = p.bs.String()
						p.bs.Reset()
					}
					p.cpMode = pmParamKey
				case '"': // -> EStr
					if p.bs.Len() > 0 {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
					p.cpmExt = epmStr
				case '_', ',', '-':
					p.bs.WriteRune(r)
				/*
					if p.bs.Len() > 0 {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				*/
				default:
					if unicode.IsDigit(r) || unicode.IsLetter(r) {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				}
			case pmParamKey:
				switch r {
				case '@':
					b.UnreadRune()
					p.cpMode = pmTag
					//					return nil, nil
				case '=': //-> ParamVal
					if p.bs.Len() > 0 {
						p.pKey = p.bs.String()
						p.bs.Reset()
						p.cpMode = pmParamVal
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				case ';', '\n', '\r', '\t', ' ': //Skipped if key is empty
					if p.bs.Len() > 0 {
						p.pKey = ""
						p.cTag.SetParam(p.bs.String(), "")
						p.bs.Reset()
					} else {
					}
				case '}':
					if p.bs.Len() > 0 {
						p.pKey = ""
						p.cTag.SetParam(p.bs.String(), "")
						p.bs.Reset()
					}
					p.cpMode = pmSkip
					t := p.cTag
					p.cTag = nil
					return t, nil
				case '{', '[', ']', ':': //Безусловный косяк
					sbc := 32
					if len(b.Bytes()) < sbc {
						sbc = len(b.Bytes()) - 1
					}
					e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
					p.cpMode = pmSkip
					return nil, e
				case '"': //-> EStr
					if p.bs.Len() > 0 {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					} else {
						p.cpmExt = epmStr
					}
				case '/':
					p.cpmExt = epmCommSt
					/*
						case '_':
							if p.bs.Len() > 0 {
								p.bs.WriteRune(r)
							} else {
								sbc := 32
								if len(b.Bytes()) < sbc {
									sbc = len(b.Bytes()) - 1
								}
								e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
								p.cpMode = pmSkip
								return nil, e
							}
					*/
				case '_', '.', ',', '-':
					p.bs.WriteRune(r)
				default:
					if unicode.IsDigit(r) || unicode.IsLetter(r) {
						p.bs.WriteRune(r)
					} else {
						sbc := 32
						if len(b.Bytes()) < sbc {
							sbc = len(b.Bytes()) - 1
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				}
			case pmParamVal:
				switch r {
				case ';', '\n', '\r': // -> Pars
					if p.bs.Len() > 0 {
						p.cTag.SetParam(p.pKey, p.bs.String())
					} else {
						p.cTag.SetParam(p.pKey, "")
					}
					p.pKey = ""
					p.bs.Reset()
					p.cpMode = pmParamKey
				case ' ', '[', ']', '@': //!!IMPOSSIBLE
					sbc := 32
					if len(b.Bytes()) < sbc {
						sbc = len(b.Bytes()) - 1
					}
					e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
					p.cpMode = pmSkip
					return nil, e
				case '"': //-> EStr
					p.cpmExt = epmStr
				case '/':
					p.cpmExt = epmCommSt
				case '_', '.', ',', '-':
					p.bs.WriteRune(r)
					/*
						if p.bs.Len() > 0 {
							p.bs.WriteRune(r)
						} else {
							sbc := 32
							if len(b.Bytes()) < sbc {
								sbc = len(b.Bytes()) - 1
							}
							e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
							p.cpMode = pmSkip
							return nil, e
						}
					*/
				default:
					if unicode.IsDigit(r) || unicode.IsLetter(r) {
						p.bs.WriteRune(r)
					} else {
						/*
							sbc := 32
							if len(b.Bytes()) < sbc {
								sbc = len(b.Bytes()) - 1
							}
							e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
						*/
						var es string
						if len(b.Bytes()) > 32 {
							es = string(b.Bytes()[:32])
						} else {
							es = string(b.Bytes())
						}
						e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + es + " in mode " + pmToString(p.cpMode))
						p.cpMode = pmSkip
						return nil, e
					}
				}
			default:
			}
		case epmCommSt:
			switch r {
			case '*':
				p.cpmExt = epmComm
			default: //Unrecognized!!!
				sbc := 32
				if len(b.Bytes()) < sbc {
					sbc = len(b.Bytes()) - 1
				}
				e := NewParseError("Unexpeted (" + string(r) + ") near: " + p.bs.String() + "_" + string(b.Bytes()[:sbc]) + " in mode " + pmToString(p.cpMode))
				p.cpMode = pmSkip
				return nil, e
			}
		case epmComm: //Комментарий
			switch r {
			case '*':
				p.cpmExt = epmCommFin
			default: //скипуем
			}
		case epmCommFin:
			switch r {
			case '/':
				p.cpmExt = epmNone
			default:
				p.cpmExt = epmComm
			}
		case epmEsc: //Esc-символ (!! может быть только в "строке")
			switch r {
			case 't':
				p.bs.WriteRune('\t')
				p.cpmExt = epmStr
			case 'r':
				p.bs.WriteRune('\r')
				p.cpmExt = epmStr
			case 'n':
				p.bs.WriteRune('\n')
				p.cpmExt = epmStr
			default: //Неизвестный символ
				p.bs.WriteRune(r)
				p.cpmExt = epmStr
			}
		case epmStr: //Строка
			switch r {
			case '\\': //Esc-символ
				p.cpmExt = epmEsc
			case '"': //Конец строки
				p.cpmExt = epmNone
			default:
				p.bs.WriteRune(r)
			}
		default:
		}
	}

	t := p.cTag
	p.cTag = nil
	p.cpMode = pmSkip
	return t, nil
}
